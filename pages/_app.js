import {useState, useEffect} from 'react';
import '../styles/globals.css';
import Layout from '../components/Layout/Layout'

import {UserProvider} from '../UserContext';
import AppHelper from '../apphelper'



function MyApp({ Component, pageProps }) {
	const [user, setUser] = useState({ email: null })

	const unsetUser = () => {
		localStorage.clear()
		setUser({ email: null })
	}

	useEffect(() => {
		if(localStorage.getItem('token')){
			const option = {
				headers: {Authorization: `Bearer${AppHelper.getAccessToken()}`}
			}
			fetch(`{AppHelper.API_URL}/users/details`, option)
			.then(res => res.json())
			.then(data => {
				if(typeof data.email != 'undefined'){
					setUser({ email: data.email })
				}else{
					setUser({ email: null })
				}

			
				})
	
		} else {
			console.log("No Authenticated User")
		}
	},[user.id])

  return (
  	<div>
		<Layout />
	  <UserProvider value={{ user, setUser, unsetUser }}>
			<Component {...pageProps} />
	  </UserProvider>
	</div>
  	)

}

export default MyApp

// https://gitlab.com/zosajuela/budget_tracker_cs3.1