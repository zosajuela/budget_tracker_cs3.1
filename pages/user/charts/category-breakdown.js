import { useState, useEffect } from 'react';
import { InputGroup, Form, Col } from 'react-bootstrap';
import { Doughnut } from 'react-chartjs-2';
import View from '../../../components/Layout/View';
import styles from '../../../components/Styles/breakdown.module.css';
import randomColor from "randomcolor";
import moment from 'moment';
import AppHelper from '../../../apphelper';


 const Category = () => {
   
    const [labelsArr, setLabelsArr] = useState([])
    const [dataArr, setDataArr] = useState([])
    const [bgColors,setBgColors] = useState([])

    const [fromDate, setFromDate] = useState(moment().subtract(1, 'months').format('YYYY-MM-DD'))
    const [toDate, setToDate] = useState(moment().format('YYYY-MM-DD'))
  
    useEffect(() => {
        const payload = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${AppHelper.getAccessToken()}`
            }, 
            body: JSON.stringify({
                fromDate: fromDate,
                toDate: toDate
            })
        }
        fetch(`${AppHelper.API_URL}/users/get-records-breakdown-by-range`, payload)
        .then(res => res.json())
        .then(data => {
            console.log(data)
            const ranData = data.map(record => randomColor())
            setBgColors(ranData)
            
            const catName = data.map(record => record.categoryName)
            setLabelsArr(catName)
            console.log(catName)

            const catAmount = data.map(record => record.totalAmount)
            console.log(catAmount)
            setDataArr(catAmount)

          
           
        })

    }, [fromDate, toDate])

  

    const data = {
        labels: labelsArr,
        datasets: [
            {
                data: dataArr,
                backgroundColor: bgColors
            }
        ]
    };

    return (
        <View title="">
        <div className={styles.cont}>
            
                   
                    <form className={styles.form}>
                    <h3>Category Breakdown</h3>
                            <label>From</label>
                            <input type="date" value={fromDate} onChange={(e) => setFromDate(e.target.value) }/>
                    
                    
                            <label>To</label>
                            <input type="date" value={toDate} onChange={(e) => setToDate(e.target.value) }/>
                    
                    </form>
           
        

            
            <div className={styles.doughnut}>
            <Doughnut data={data} height={20} width={20}/>
            </div>
           
            </div>
        </View>

    )
}

export default Category;

// // https://boodle.zuitt.co/notes/723

//     // <form className={styles.form}>
//             //     <div>
//             //         <label>From</label>
//             //         <input type="date"
//             //          value={ fromDate } 
//             //          onChange={ (e) => setFromDate(e.target.value) }/>
//             //     </div>
//             //     <div>
//             //         <label>To</label>
//             //         <input type="date"
//             //          value={ toDate }
//             //           onChange={ (e) => setToDate(e.target.value) }/>
//             //     </div>
//             // </form>