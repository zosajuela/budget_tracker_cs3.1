import { useState, Fragment } from 'react';
import { Card, Button, Row, Col, InputGroup, FormControl, Form } from 'react-bootstrap'
import Link from 'next/link';
import View from '../../../components/Layout/View';
import styles from '../../../components/Styles/newCategory.module.css';

export default () => {
    const [searchKeyword, setSearchKeyword] = useState('')
    const [searchType, setSearchType] = useState("All")

    return (
        <View title="">
        <div className={styles.form_line}>
            <h3 className={styles.title}>Add Records</h3>
           
    
              
                <input 
                className={styles.search}
                placeholder="Search Record"
                 value={searchKeyword} 
                 onChange={(e) => setSearchKeyword(e.target.value) }/>

                <select
                className={styles.search}
                as="select" 
                defaultValue={searchType} 
                onChange={(e) => setSearchType(e.target.value) }>
                
                    <option value="All">All</option>
                    <option value="Income">Income</option>
                    <option value="Expense">Expense</option>
                </select>
           
            <RecordsView searchKeyword={ searchKeyword } searchType={ searchType }/>
            </div>
        </View>
    )
}

const RecordsView = () => {
    const [records, setRecords] = useState([])

       
    return (
        <Fragment>
            {
                records.map((record) => {
                    const textColor = (record.type === 'Income') ? 'text-success' : 'text-danger'
                    const amountSymbol = (record.type === 'Income') ? '+' : '-'

                    return (
                        <Card className="mb-3" key={ record._id }>
                            <Card.Body>
                                <Row>
                                    <Col xs={ 6 }>
                                        <h5>{ record.description }</h5>
                                        <h6><span className={ textColor }>{ record.type }</span> { ' (' + record.categoryName + ')' }</h6>
                                        <p>{ moment(record.dateAdded).format("MMMM D, YYYY") }</p>
                                    </Col>
                                    <Col xs={ 6 } className="text-right">
                                        <h6 className={ textColor }>{ amountSymbol + ' ' + record.amount.toLocaleString() }</h6>
                                        <span className={ textColor }>{ record.balanceAfterTransaction.toLocaleString() }</span>
                                    </Col>
                                </Row>
                            </Card.Body>
                        </Card>
                    )
                })
            }
        </Fragment>
    )
}
