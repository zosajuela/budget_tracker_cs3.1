import { useState, useEffect } from 'react';
import { Form, Table , Row, Col, Card } from 'react-bootstrap';
import Router from 'next/router';
import View from '../../../components/Layout/View';
import Swal from 'sweetalert2';
import styles from '../../../components/Styles/newchart.module.css';
import AppHelper from '../../../apphelper';


 const NewRecordForm = () => {
    const [categoryName, setCategoryName] = useState(undefined)
    const [typeName, setTypeName] = useState(undefined)
    const [amount, setAmount] = useState(0)
    const [description, setDescription] = useState('')
    const [categories, setCategories] = useState([])
   
const [dateA,setDateA] = useState()
    


    const newRecord = (e) => {
        e.preventDefault()

        const payload = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${AppHelper.getAccessToken()}`
            },
            body: JSON.stringify({
                categoryName: categoryName,
                type: typeName,
                  amount: amount,
                 description: description
            })
    }

    fetch(`${AppHelper.API_URL}/users/add-record`, payload)
    .then(res => res.json())
    .then(data => {
        if(data == true){
            console.log(data)
            Swal.fire({
                icon: "success",
                title: "Successfully added the Record",
                text: "Thank you!."
            })

            location.reload()
        }else{
            Swal.fire({
                icon: "error",
                title: "Registration failed",
                text: "Something went wrong"
                 })

             }
        })
    }

    useEffect(() => {
        const payload = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${AppHelper.getAccessToken()}`
            }
        }
        fetch(`${AppHelper.API_URL}/users/get-categories`, payload)
        .then(res => res.json())
        .then(data => {
           console.log(data)
           setCategories(data)
        })
     },[])

     

    //  useEffect(() => {
    //     const payload = {
    //         method: 'POST',
    //         headers: {
    //             'Content-Type': 'application/json',
    //             'Authorization': `Bearer ${AppHelper.getAccessToken()}`
    //         }
    //     }
    //     fetch(`${AppHelper.API_URL}/users/get-transactions`, payload)
    //     .then(res => res.json())
    //     .then(transData => {
    //        console.log(transData)
        
    //        const date = transData.map(record => record.dateAdded)
    //        console.log(date)
    //        setDateA(date)
           
    // const catType = transData.map((transac) => {
    //     return transac.type
    // })
    //     for (let i = 0; i <= catType.length; i++) {
    //         console.log(i)
        
    //     }


    //     })
    //  },[])

    return (
        <View title="">
        <div>
        <div  className={styles.containernew}>
        <form 
       className={styles.box}
        onSubmit={(e) => newRecord(e)}>

         <div className={styles.boxer}>
         <div className={styles.blur}>
         <label className={styles.label}>New Record</label>
         <div className={styles.margin}></div>
                <label className={styles.label}>Category Type:</label>
                
                <div className={styles.margin}></div>
                <Form.Control 
                className={styles.control}
                as="select" 
                value={typeName} 
                onChange={(e) => setTypeName(e.target.value)} required>
                            <option value selected disabled>Select Category</option>
                            <option value="">n/a</option>
                            <option value="Income">Income</option>
                            <option value="Expense">Expense</option>
                </Form.Control>
                <div className={styles.margin}></div>
            
            <Form.Group controlId="categoryName">
                <label className={styles.label}>Category Name:</label>

                <div className={styles.margin}></div>

                <Form.Control as="select" 
                value={categoryName} 
                onChange={(e) => setCategoryName(e.target.value) } required>

                    <option value selected disabled>Select Category Name</option>

                    {
                        categories.map((category) => {
                            return (
                                <option key={category._id} value={category.name}>{category.name}</option>
                            )
                        })
                    }

                </Form.Control>
            </Form.Group>
            <div className={styles.margin}></div>
            
                <label className={styles.label}>Amount:</label>
                <div className={styles.margin}></div>
                <input type="number" placeholder="Enter amount"
                 value={amount} 
                 onChange={(e) => setAmount(parseFloat(e.target.value)) } required/>
                 <div className={styles.margin}></div>
                <label className={styles.label}>Description:</label>
                <div className={styles.margin}></div>
                <input type="text" placeholder="Enter description"
                 value={description} 
                 onChange={(e) => setDescription(e.target.value) } required/>
              
                 <div className={styles.margin}></div>
            <input className={styles.btn} variant="primary" type="submit" />
            </div>
            </div>
        </form>
    
 
        </div>
        </div>
          
        </View>
    )
}


export default NewRecordForm