import { useState, useEffect } from 'react';
import { Form, Button, Row, Col, Card } from 'react-bootstrap';
// import Router from 'next/router'
import Swal from 'sweetalert2';
import View from '../../../components/Layout/View';
import styles from '../../../components/Styles/newchart.module.css';
import AppHelper from '../../../apphelper';

 const NewCategoryForm = () => {
   
    const [categoryName, setCategoryName] = useState('')
        const [typeName, setTypeName] = useState('')
      
        
        const createCategory = (e) => {
            e.preventDefault()

            const payload = {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${AppHelper.getAccessToken()}`
                },
                body: JSON.stringify({
                    name: categoryName,
                    type: typeName  
                })
        }

        
    
        fetch(`${AppHelper.API_URL}/users/add-category`, payload)
            .then(res => res.json())
            .then(data => { 
                if(data == true){
                    Swal.fire({
                        icon: "success",
                        title: "Successfully added the Record",
                        text: "Thank you!."
                    })
                }else{
                    Swal.fire({
                        icon: "error",
                        title: "Failed to add Record",
                        text: "Something went wrong"
                    })
    
                }
            })
            

        }

    return (
        <View title="">
            
        <div  className={styles.containernew}>
                  <form
                  className={styles.box}
                   onSubmit={ (e) => createCategory(e) }>
                   <div className={styles.boxer}>
                   <div className={styles.blur}>

                   <div className={styles.margin}></div>

                <label className={styles.label}>Category Name:</label>

                <div className={styles.margin}></div>

                <input 
                type="text" 
                placeholder="Enter category name" 
                value={categoryName} 
                onChange={(e) => setCategoryName(e.target.value)} required/>
         
                <div className={styles.margin}></div>

            <label className={styles.label}>Category Type:</label>

        <div className={styles.margin}></div>
        <div className={styles.margin}></div>

                            <select
                            value={typeName} 
                            onChange={(e) => setTypeName(e.target.value)}>
                                <option value selected disabled>Select Category</option>
                                <option value="">n/a</option>
                                <option value="Income">Income</option>
                                <option value="Expense">Expense</option>
                            </select>

            <input className={styles.btn} variant="primary" type="submit"/>

            <div className={styles.margin}></div>
            <div className={styles.margin}></div>
            </div>
            </div>
        </form>

        </div>
        </View>
    )
}

export default NewCategoryForm;

// const NewCategoryForm = () => {
//     const [categoryName, setCategoryName] = useState('')
//     const [typeName, setTypeName] = useState('')

//     return (
//         <Form onSubmit={ (e) => createCategory(e) }>
//             <Form.Group controlId="categoryName">
//                 <Form.Label>Category Name:</Form.Label>
//                 <Form.Control type="text" placeholder="Enter category name" 
//                 value={categoryName} 
//                 onChange={ (e) => setCategoryName(e.target.value) } required/>
//             </Form.Group>
//             <Form.Group controlId="typeName">
//                 <Form.Label>Category Type:</Form.Label>
//                 <Form.Control as="select"
//                  value={ typeName }
//                   onChange={ (e) => setTypeName(e.target.value) } required>
//                     <option value selected disabled>Select Category</option>
//                     <option value="Income">Income</option>
//                     <option value="Expense">Expense</option>
//                 </Form.Control>
//             </Form.Group>
//             <Button variant="primary" type="submit">Submit</Button>
//         </Form>
//     )
// }

