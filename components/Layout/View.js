import Head from 'next/head'
import { motion } from 'framer-motion'


const View = ({title, children}) => {
    return(
        <div>
            <Head >
            <titl key="title-tag">{title}</titl>
            <link rel="icon" href="/covid.png" />
            <meta meta key="title-meta" name="viewport" content="initial-scale=1.0, width=device-width"/>
            </Head>
                    <div className="title">{children}</div>
                  
           
        </div>
    )
}

export default View;