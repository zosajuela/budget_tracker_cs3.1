import Link from "next/link";
import { useEffect, useState } from "react";
import { Brightness6Rounded, TramRounded, TrendingUpRounded } from "@material-ui/icons";
import styles from "./Layout.module.css";
import { motion, AnimatePresence } from 'framer-motion';
import { useRouter } from 'next/router';
import Swal from 'sweetalert2'


const Layout = () => {
  const [theme, setTheme] = useState("light");
  let [color, setColor] = useState(false);

  useEffect(() => {
    document.documentElement.setAttribute(
      "data-theme",
      localStorage.getItem("theme")
    );

    setTheme(localStorage.getItem("theme"));
  }, []);

  useEffect(() => {
    document.documentElement.setAttribute(
      "data-color",
      localStorage.getItem("color")
    );

    setColor(localStorage.getItem("color"));
  }, []);

  const switchTheme = () => {
    if (theme === "light") {
      saveTheme("dark");
    } else {
      saveTheme("light");
    }if(theme !== "light"){
      saveColor("black")
    }else{
      saveColor("white")
    }
  };


  const saveTheme = (theme) => {
    setTheme(theme);
    localStorage.setItem("theme", theme);
    document.documentElement.setAttribute("data-theme", theme);
  };
  const saveColor = (color) => {
      setColor(color);
      localStorage.setItem("color", color);
      document.documentElement.setAttribute("data-color", color);
  };

  const svgVariants = {
    hidden: { 
      rotate: -360,
      duration: 2 
    },
    visible: { 
      rotate: 360, 
      transition: { 
        repeat: Infinity, 
        duration: 2 }
    }
  }
const pngVariants = {
  hidden: { 
    rotate: 360,
    duration: 2 
  },
  visible: { 
    rotate: -360, 
    transition: { 
      repeat: Infinity, 
      duration: 18 }
  }
}
  const pathVariants = {
    hidden: {
      opacity: 0,
    },
    visible: {
      opacity: 1,
      transition: {
        duration: 2,
        ease: "easeInOut"
      }
    }
  }




  const containerVariants = {
    hidden: {
        x: '-100vw',
    },
    visible: {
        x: 0,
        transition: {
            delay: 0.5, duration: .5
        }
    },
}


const hideVariants = {
    hidden: {
        x: 0,
    },
    visible: {
        x: '-100vw',
        transition: {
            delay: 0.5, duration: .5
        }
    },
    exit: {
      x: 0
    }
}


let [button, setButton] = useState(true)



const [log, setLog] = useState(true)
const route = useRouter();

useEffect(() =>{
  let userToken = localStorage.getItem("token")
  if(userToken) {
    setLog(false)
    
  }else{
    setLog(true)
    route.push("/login")
  }
}, [])


const logOut = (e) => {
  e.preventDefault()
  let userToken = localStorage.getItem("token")
  if(userToken){
    Swal.fire({
      icon: "success",
      title: "Logout",
      text: "Successful!"
    })
    route.push("/login")
    setButton(true)
    setLog(true)
  localStorage.clear() 
  }else{
    setButton(false)
    Swal.fire({
      icon: "error",
      title: "Opss!",
      text: "Something went wrong"
    })
  }
}




const showClick = (e) => {
  e.preventDefault()
  if(button === false){
    return setButton(true)
  }else{
    return setButton(false)
  }
}


  return (
    <div className={styles.container}>
     
    <div className={styles.topborder}> </div>
      <header className={styles.header}>

            
           
                <div className={styles.boxL}>
                    <div className={button ? styles.menu_man : styles.menu} 
                    onClick={(e) => showClick(e)}>
                    
                      <div className={styles.menu_burger}> </div>
                  </div>
                </div>
              
          
            <div className={styles.clock}>
                <motion.img className={styles.logo} src="/clock.png" alt="budget"
                variants={svgVariants}
                initial="hidden"
                animate="visible" />
                <motion.img className={styles.logo} src="/clocky.png" alt="budget"
                variants={pngVariants}
                initial="hidden"
                animate="visible" />
            </div>

          

                <Link href="/">
                  <a>
              <h2 className={styles.h2}>ZBUDGET  TRACKER</h2>
                  </a>
                </Link>

                <button className={styles.themeSwitcher} onClick={switchTheme}>
                  <Brightness6Rounded />
                </button>
                
              
                {log ? (

                  <div className={styles.loginout}>
                  <Link href="/login">
                        <a> LOG IN</a>
                  </Link>
                  </div>
                  
                  )  
                  :
                   ( 
                     <div className={styles.loginout}>
                    <small onClick={(e) => logOut(e)}>LOG Out</small>
                    <br/>
                 </div>
                  )}

                
              
          

      </header>
         

        <div className={styles.Nav}>
          <AnimatePresence>
        
                <motion.div className={styles.Navi}
                variants={button ? hideVariants : containerVariants}
                initial="hidden"
                animate="visible"
                exit
                >
                  <Link href="/user/categories/new">
                      <a className={styles.link} id={styles.link1}  role="button">Categories</a>
                  </Link>

                     <div className={styles.border}></div>

                  <Link href="/user/records/new">
                      <a className={styles.link} id={styles.link2}  role="button">Records</a>
                  </Link>

                  <div className={styles.border}></div>

                
                  <Link href="/MonthlyExpense">
                  <a className={styles.link} id={styles.link3}  role="button">Monthly Expense</a>
               </Link>
                    
                      <div className={styles.border}></div>

                  <Link href="/MonthlyExpense">
                  <a className={styles.link} id={styles.link4} role="button">Monthly Income</a>
                </Link>

                     <div className={styles.border}></div>

                <Link href="/MonthlyExpense">
                  <a className={styles.link} id={styles.link5} role="button">Trend</a>
                </Link>

                      <div className={styles.border}></div>

                  <Link href="/user/charts/category-breakdown">
                      <a className={styles.link} id={styles.link6} role="button">Breakdown</a>
                  </Link>
                  
                </motion.div>
            </AnimatePresence>
        </div>  
        <div className={styles.topborder}> </div>
    </div>
  );
};

export default Layout;


